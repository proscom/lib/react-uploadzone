# React Uploadzone

Компонент, добавляющий в [react-dropzone](https://github.com/react-dropzone/react-dropzone/)
асинхронную загрузку файлов на бекенд.

## Установка

```
npm install --save react@16 react-dropzone
npm install --save @proscom/react-uploadzone
```

## Использование

```jsx harmony
<UploadZone
  maxFiles={3} // максимальное число файлов
  accept="image/jpeg, image/png" // mime-типы файлов или расширения
  maxFileSize={1024 * 1024} // максимальный размер одного файла
  uploadFile={jqueryUpload} // функция для загрузки файла на сервер
>
{({
  getRootProps, // пропы dropzone контейнера
  getInputProps, // пропы dropzone инпута
  isDragActive, // перетягивает ли пользователь файлы
  files, // список загружаемых / загруженных файлов
  removeFile, // функция удаления файла
  cancelUpload // функция отмены загрузки файла
}) => {
  return (
    <div className="uploadzone">
      <div
        {...getRootProps()}
        className={cn('dropzone', {
          'dropzone--isActive': isDragActive
        })}
      >
        <input {...getInputProps()} />
        {isDragActive ? (
          <p>Drop files here...</p>
        ) : (
          <p>
            Try dropping some files here, or click to select files to
            upload.
          </p>
        )}
      </div>
      <div className="uploadzone__files">
        {files.map((file) => {
          let status = 'Attached';
          if (file.isUploading) {
            status = 'Uploading';
          }
          if (file.isUploaded) {
            status = 'Uploaded';
          }
          if (file.error) {
            status = 'Error: ' + file.error;
          }

          return (
            <div className="file" key={file.id}>
              <div className="file__name">{file.nativeFile.name}</div>
              <div className="file__status">{status}</div>
              <div className="file__progress">{file.progress}</div>
              <div className="file__response">
                {JSON.stringify(file.response)}
              </div>
              {file.isUploading ? (
                <button
                  className="file__cancel"
                  onClick={() => cancelUpload(file.id)}
                >
                  Cancel
                </button>
              ) : (
                false
              )}
              {file.isUploaded ? (
                <button
                  className="file__remove"
                  onClick={() => removeFile(file.id)}
                >
                  Remove
                </button>
              ) : (
                false
              )}
            </div>
          );
        })}
      </div>
    </div>
  );
}}
</UploadZone>
``` 

Пример функции загрузки файлов на сервер с помощью jquery:

```typescript
  function jqueryUpload({ file, onSuccess, onError, onProgress }) {
    let formdata = new FormData();
    formdata.append('file', file.nativeFile, file.nativeFile.name);

    return $.ajax({
      url: '/upload/file',
      method: 'POST',
      data: formdata,
      dataType: 'json',
      processData: false,
      contentType: false,
      cache: false,
      success: (data) => {
        onSuccess(data.id);
      },
      xhr: () => {
        let xhr = new window.XMLHttpRequest();
        xhr.upload.addEventListener('progress', (e) => {
          if (e.lengthComputable) {
            onProgress(e.loaded, e.total);
          }
        }, false);
        return xhr;
      },
      error: (xhr, status, error) => {
        if (xhr.statusText !== 'abort') {
          onError(error.message);
        }
      }
    });
  }
```
