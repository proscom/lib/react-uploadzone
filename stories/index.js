import React from 'react';
import { storiesOf } from '@storybook/react';
import {
  FileRejectedError,
  UploadZone,
  RejectionReason,
  FileUploadError,
  MaxFilesError
} from '../src/UploadZone';
import cn from 'classnames';
import './index.css';

function stubUpload({ file, onSuccess, onError, onProgress }) {
  let steps = 10;
  let step = 0;
  let aborted = false;

  function tick() {
    if (aborted) return;
    console.log(file, step, steps);
    if (step < steps) {
      step++;
      onProgress(step, steps);
      setTimeout(tick, 1000);
    } else {
      onSuccess({ id: 5 });
    }
  }

  setTimeout(tick, 1000);

  return {
    abort: () => {
      aborted = true;
    }
  };
}

class ErrorWrapper extends React.Component {
  state = {
    errors: []
  };

  errorIdSequence = 0;

  handleError = (error) => {
    const newError = {
      id: this.errorIdSequence++,
      text: 'Generic error'
    };

    if (error instanceof FileRejectedError) {
      if (error.reason === RejectionReason.MIME_TYPE) {
        newError.text = 'Wrong file type';
      } else if (error.reason === RejectionReason.MAX_FILE_SIZE) {
        newError.text = 'File size error';
      }
    } else if (error instanceof FileUploadError) {
      newError.text = 'Upload error';
    } else if (error instanceof MaxFilesError) {
      newError.text = 'Max files';
    }

    this.setState({
      errors: [...this.state.errors, newError]
    });

    setTimeout(() => {
      this.setState((state) => ({
        errors: state.errors.filter((e) => e !== newError)
      }));
    }, 5000);
  };

  render() {
    return (
      <div>
        {this.props.children({ handleError: this.handleError })}
        <div className="errors">
          {this.state.errors.map((e, i) => (
            <div key={i} className="error">
              {e.text}
            </div>
          ))}
        </div>
      </div>
    );
  }
}

storiesOf('UploadZone', module).add('basic', () => (
  <ErrorWrapper>
    {({ handleError }) => (
      <UploadZone
        maxFiles={3}
        accept="image/jpeg"
        maxFileSize={1024 * 1024}
        onError={handleError}
        uploadFile={stubUpload}
      >
        {({
          getRootProps,
          getInputProps,
          isDragActive,
          files,
          removeFile,
          cancelUpload
        }) => {
          return (
            <div className="uploadzone">
              <div
                {...getRootProps()}
                className={cn('dropzone', {
                  'dropzone--isActive': isDragActive
                })}
              >
                <input {...getInputProps()} />
                {isDragActive ? (
                  <p>Drop files here...</p>
                ) : (
                  <p>
                    Try dropping some files here, or click to select files to
                    upload.
                  </p>
                )}
              </div>
              <div className="uploadzone__files">
                {files.map((file) => {
                  let status = 'Attached';
                  if (file.isUploading) {
                    status = 'Uploading';
                  }
                  if (file.isUploaded) {
                    status = 'Uploaded';
                  }
                  if (file.error) {
                    status = 'Error: ' + file.error;
                  }

                  return (
                    <div className="file" key={file.id}>
                      <div className="file__name">{file.nativeFile.name}</div>
                      <div className="file__status">{status}</div>
                      <div className="file__progress">{file.progress}</div>
                      <div className="file__response">
                        {JSON.stringify(file.response)}
                      </div>
                      {file.isUploading ? (
                        <button
                          className="file__cancel"
                          onClick={() => cancelUpload(file.id)}
                        >
                          Cancel
                        </button>
                      ) : (
                        false
                      )}
                      {file.isUploaded ? (
                        <button
                          className="file__remove"
                          onClick={() => removeFile(file.id)}
                        >
                          Remove
                        </button>
                      ) : (
                        false
                      )}
                    </div>
                  );
                })}
              </div>
            </div>
          );
        }}
      </UploadZone>
    )}
  </ErrorWrapper>
));
